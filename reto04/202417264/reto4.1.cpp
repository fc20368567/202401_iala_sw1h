#include <iostream>
#include <cstdlib> 
#include <ctime>

using namespace std;

inline int randint(int a, int b) { return rand() % (b - a) + a; }

int* generarArregloAleatorio(int& tam) {
    srand(time(nullptr)); 

    tam = randint(100, 500);
    int* arreglo = new int[tam];

    for (int i = 0; i < tam; ++i) {
        arreglo[i] = randint(1, 10000);
    }

    cout << "Tamanio del arreglo generado: " << tam << endl; // Imprimir el tama�o del arreglo generado

    return arreglo;
}

int main() {
    int tam;
    int* arreglo = generarArregloAleatorio(tam);

    cout << "Arreglo aleatorio generado:" << endl;
    for (int i = 0; i < tam; ++i) {
        cout << arreglo[i] << " ";
    }
    cout << "\n";
    system("pause");
    delete[] arreglo; // Liberar la memoria asignada al arreglo
    return 0;
}
