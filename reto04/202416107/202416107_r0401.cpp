#include <iostream>
#include <random>
#include <stdio.h>

using namespace std;

int randint(int min, int max, random_device& rd) {
    mt19937_64 gen(rd());
    return gen() % (max - min) + min;
}

int* randarray(int* num) {
    random_device rd;
    *num = randint(100, 501, rd);
    int* array = new int[*num];

    for (int i = 0; i < *num; ++i) {
        array[i] = randint(1, 10001, rd);
    }
    return array;
}

/*CODIGO DE PRUEBA
int main() {
    int *num = new int;
    int* array = randarray(num);

    cout << "El tamanio del arreglo es: " << *num << '\n' << endl;

    cout << "Los elementos del arreglo son: " << '\n' << endl;
    for (int i = 0; i < *num; ++i) {
        cout << array[i] << " ";
    }

    delete num;
    delete []array;
    system("pause>0");
    return EXIT_SUCCESS;
}
*/